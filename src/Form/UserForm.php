<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class UserForm
 */
class UserForm extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
            ->add('name', TextType::class, ['label' => 'Felhasználónév', 'required' => false, 'attr' => ['class' => 'form-control', 'placeholder' => 'Felhasználónév']])
            ->add('password', PasswordType::class, ['label' => 'Jelszó', 'required' => false, 'attr' => ['class' => 'form-control', 'placeholder' => 'Jelszó']])
            ->add('submit', SubmitType::class, ['label' => 'Belépés', 'attr' => ['class' => 'btn btn-primary']]);
    }

    /**
     * @param OptionsResolver $resolver
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver) {

        $resolver->setDefaults([
            'data_class' => User::class
        ]);
    }
}