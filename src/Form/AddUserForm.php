<?php

namespace App\Form;

use App\Constant\Project;
use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AddUserForm
 */
class AddUserForm extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
            ->add('name', TextType::class, ['label' => 'Felhasználónév', 'required' => false, 'attr' => ['class' => 'form-control', 'placeholder' => 'Felhasználónév']])
            ->add('email', EmailType::class, ['label' => 'E-mail cím', 'required' => false, 'attr' => ['class' => 'form-control', 'placeholder' => 'E-mail cím']])
            ->add('password', RepeatedType::class, array(
                'type' => PasswordType::class,
                'required' => false,
                'invalid_message' => Project::NOT_SAME_PASSWORD_ERROR_MSG,
                'first_options'  => array('label' => 'Jelszó', 'attr' => ['class' => 'form-control', 'placeholder' => 'Jelszó']),
                'second_options' => array('label' => 'Jelszó megerősítése', 'attr' => ['class' => 'form-control', 'placeholder' => 'Jelszó megerősítése'])
                )
            )
            ->add('submit', SubmitType::class, ['label' => 'Rögzítés', 'attr' => ['class' => 'btn btn-primary']]);
    }

    /**
     * @param OptionsResolver $resolver
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver) {

        $resolver->setDefaults(['data_class' => User::class]);
    }
}