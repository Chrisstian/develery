<?php

namespace App\Form;

use App\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ContactForm
 */
class AddContactForm extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
            ->add('name', TextType::class, ['label' => 'Neved', 'required' => false, 'attr' => ['class' => 'form-control', 'placeholder' => 'Neved']])
            ->add('email', TextType::class, ['label' => 'E-mail címed', 'required' => false, 'attr' => ['class' => 'form-control', 'placeholder' => 'E-mail címed']])
            ->add('message', TextareaType::class, ['label' => 'Üzenet szövege', 'required' => false, 'attr' => ['class' => 'form-control no-resize', 'placeholder' => 'Üzenet szövege', 'rows' => '5']])
            ->add('submit', SubmitType::class, ['label' => 'Küldés', 'attr' => ['class' => 'btn btn-primary']]);
    }

    /**
     * @param OptionsResolver $resolver
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver) {

        $resolver->setDefaults(['data_class' => Contact::class]);
    }
}