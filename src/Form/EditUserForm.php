<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class EditUserForm
 */
class EditUserForm extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
            ->add('name', TextType::class, ['label' => 'Felhasználónév', 'required' => true, 'attr' => ['class' => 'form-control', 'placeholder' => 'Felhasználónév']])
            ->add('email', EmailType::class, ['label' => 'E-mail cím', 'required' => true, 'attr' => ['class' => 'form-control', 'placeholder' => 'E-mail cím']])
            ->add('isActive', ChoiceType::class, ['label' => 'Státusz', 'required' => true, 'choices' => ['Aktív' => true, 'Inaktív' => false], 'attr' => ['class' => 'form-control', 'placeholder' => 'Státusz']])
            ->add('createdAt', DateTimeType::class, ['label' => 'Létrehozás ideje', 'required' => true, 'widget' => 'single_text', 'attr' => ['class' => 'form-control', 'placeholder' => 'Létrehozás ideje', 'readonly' => true]])
            ->add('submit', SubmitType::class, ['label' => 'Módosítás', 'attr' => ['class' => 'btn btn-primary']]);
    }

    /**
     * @param OptionsResolver $resolver
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver) {

        $resolver->setDefaults(['data_class' => User::class]);
    }
}