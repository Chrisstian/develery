<?php

namespace App\Controller;

use App\Constant\Project;
use App\Entity\Contact;
use App\Form\AddContactForm;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Monolog\DateTimeImmutable;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ContactController
 */
class ContactController extends AbstractController {

    /**
     * Új üzenet rögzítése
     *
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return Response
     * @throws Exception
     */
    #[Route('/add-contact', name: 'addContact', methods: ['POST', 'GET'])]
    public function addContact(Request                $request,
                               EntityManagerInterface $entityManager): Response {

        $contact = new Contact();
        $form = $this->createForm(AddContactForm::class, $contact);

        try {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $this->requestValidation($form->getData());

                $formData = $form->getData();
                date_default_timezone_set('Europe/Budapest');
                $contact->setName($formData->getName());
                $contact->setEmail($formData->getEmail());
                $contact->setMessage($formData->getMessage());
                $contact->setCreatedAt(new DateTimeImmutable(date('Y-m-d H:i:s')));
                $entityManager->persist($contact);
                $entityManager->flush();
                $this->addFlash('success', Project::CONTACT_SUCCESSFUL_MSG);
            }
        } catch (Exception $e) {
            $this->addFlash('error', $e->getMessage());
        }

        return $this->render('Contact/newContact.html.twig', ['form' => $form]);
    }

    /**
     * Kontaktok kilistázása
     *
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    #[Route('/contacts', name: 'contacts', methods: 'GET')]
    public function contacts(EntityManagerInterface $entityManager): Response {

        $contacts = array();

        try {
            $contacts = $entityManager->getRepository(Contact::class)->findAll();
        } catch (Exception $e) {
            $this->addFlash('error', $e->getMessage());
        }

        return $this->render('Contact/contact.html.twig', ['form' => $contacts]);
    }

    /**
     * Form adatok validálása
     *
     * @param object $formData
     * @return void
     * @throws Exception
     */
    private function requestValidation(object $formData): void {

        if(empty($formData->getName()) || empty($formData->getEmail()) || empty($formData->getMessage())) {
            throw new Exception(Project::EMPTY_FIELD_ERROR_MSG);
        } else if(!filter_var($formData->getEmail(), FILTER_VALIDATE_EMAIL)) {
            throw new Exception(Project::NOT_VALID_EMAIL_ERROR_MSG);
        }
    }
}