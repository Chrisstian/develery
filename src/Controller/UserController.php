<?php

namespace App\Controller;

use App\Constant\Project;
use App\Entity\User;
use App\Form\AddUserForm;
use App\Form\EditUserForm;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Monolog\DateTimeImmutable;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 */
class UserController extends AbstractController {

    /**
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    #[Route('/users', name: 'users', methods: 'GET')]
    public function user(EntityManagerInterface $entityManager): Response {

        $users = array();

        try {
            $users = $entityManager->getRepository(User::class)->findAll();
        } catch (Exception $e) {
            $this->addFlash('error', $e->getMessage());
        }

        return $this->render('User/user.html.twig', ['form' => $users]);
    }

    /**
     * Új felhasználó rögzítése
     *
     * @param Request $request
     * @param UserPasswordHasherInterface $passwordHasher
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    #[Route('/add-user', name: 'addUser', methods: ['POST', 'GET'])]
    public function addUser(Request                     $request,
                            UserPasswordHasherInterface $passwordHasher,
                            EntityManagerInterface      $entityManager): Response {

        $user = new User();
        $form = $this->createForm(AddUserForm::class, $user);

        try {
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $formData = $form->getData();

                if(empty($formData->getName()) || empty($formData->getEmail())) {
                    throw new Exception(Project::EMPTY_FIELD_ERROR_MSG);
                } else {
                    date_default_timezone_set('Europe/Budapest');
                    $hashedPassword = $passwordHasher->hashPassword($user, $formData->getPassword());
                    $user->setName($formData->getName());
                    $user->setEmail($formData->getEmail());
                    $user->setRoles($formData->getRoles());
                    $user->setPassword($hashedPassword);
                    $user->setIsActive(Project::IS_ACTIVE);
                    $user->setCreatedAt(new DateTimeImmutable(date('Y-m-d H:i:s')));
                    $entityManager->persist($user);
                    $entityManager->flush();
                    $this->addFlash('success', Project::USER_SUCCESSFUL_MSG);
                }
            }
        } catch (Exception $e) {
            $this->addFlash('error', $e->getMessage());
        }

        return $this->render('User/newUser.html.twig', ['form' => $form]);
    }

    /**
     * Felhasználó módosítása
     *
     * @param Request $request
     * @param int $userId
     * @param UserPasswordHasherInterface $passwordHasher
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    #[Route('/edit-user/{userId}', name: 'editUser', methods: ['GET', 'POST'])]
    public function editUser(Request                     $request,
                             int                         $userId,
                             UserPasswordHasherInterface $passwordHasher,
                             EntityManagerInterface      $entityManager): Response {

        $user = new User();
        $form = $this->createForm(EditUserForm::class, $user);

        try {
            $criteria = array('id' => $userId);
            $userData = $entityManager->getRepository(User::class)->findOneBy($criteria);

            if(empty($userData)) {
                throw new Exception(Project::INVALID_PARAMETER_ERROR_MSG);
            } else {
                $form = $this->createForm(EditUserForm::class, $userData);
                $form->handleRequest($request);
                if ($form->isSubmitted() && $form->isValid()) {
                    date_default_timezone_set('Europe/Budapest');
                    $userData->setName($userData->getName());
                    $userData->setIsActive($userData->isIsActive());
                    $userData->setModifiedAt(new DateTimeImmutable(date('Y-m-d H:i:s')));
                    $entityManager->persist($userData);
                    $entityManager->flush();
                    $this->addFlash('success', Project::EDIT_SUCCESSFUL_MSG);
                }
            }
        } catch (Exception $e) {
            $this->addFlash('error', $e->getMessage());
        }

        return $this->render('User/editUser.html.twig', ['form' => $form]);
    }

    /**
     * Felhasználó törlése
     *
     * @param int $userId
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    #[Route('/delete-user/{userId}', name: 'deleteUser', methods: ['GET'])]
    public function deleteUser(int                    $userId,
                               EntityManagerInterface $entityManager): Response {

        $users = array();

        try {
            $criteria = array('id' => $userId);
            $userData = $entityManager->getRepository(User::class)->findOneBy($criteria);

            if(empty($userData)) {
                throw new Exception(Project::INVALID_PARAMETER_ERROR_MSG);
            } else {
                $entityManager->remove($userData);
                $entityManager->flush();
            }

            $users = $entityManager->getRepository(User::class)->findAll();
        } catch (Exception $e) {
            $this->addFlash('error', $e->getMessage());
        }

        return $this->render('User/user.html.twig', ['form' => $users]);
    }
}