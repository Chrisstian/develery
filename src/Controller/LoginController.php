<?php

namespace App\Controller;

use App\Constant\Project;
use App\Entity\User;
use App\Form\LoginForm;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Class LoginController
 */
class LoginController extends AbstractController {

    /**
     * Bejelentkezés
     *
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    #[Route('/login', name: 'user_login', methods: ['POST','GET'])]
    public function login(AuthenticationUtils $authenticationUtils): Response {

        $user = new User();
        $form = $this->createForm(LoginForm::class, $user);

        try {
            $error = $authenticationUtils->getLastAuthenticationError();
            if(is_object($error) && !empty($error)) {
                if($error->getMessage()) {
                    throw new Exception(Project::NOT_VALID_LOGIN_ERROR_MSG);
                }
            }
        } catch (Exception $e) {
            $this->addFlash('error', $e->getMessage());
        }

        return $this->render('Login/login.html.twig', ['form' => $form]);
    }
}