<?php

namespace App\Constant;

/**
 * Class Project
 */
class Project {

    public const EMPTY_FIELD_ERROR_MSG = 'Hiba! Kérjük töltsd ki az összes mezőt!';
    public const NOT_VALID_EMAIL_ERROR_MSG = 'Hiba! Kérjük e-mail címet adjál meg!';
    public const NOT_VALID_LOGIN_ERROR_MSG = 'Rossz felhasználónév vagy jelszó!';
    public const NOT_SAME_PASSWORD_ERROR_MSG = 'A két jelszó eltérő!';
    public const CONTACT_SUCCESSFUL_MSG = 'Köszönjük szépen a kérdésedet. Válaszunkkal hamarosan keresünk a megadott e-mail címen.';
    public const INVALID_PARAMETER_ERROR_MSG = 'Paraméter hiba!';
    public const USER_SUCCESSFUL_MSG = 'Sikeres felhasználó felvétel';
    public const EDIT_SUCCESSFUL_MSG = 'Sikeres adatmódosítás';

    public const IS_ACTIVE = 1;
}